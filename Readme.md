---
title: Spike Report
---

Core 2 – C++ Basics
===================

Introduction
------------

We wish to use Unreal Engine’s C++ to code throughout the semester, but
we do not have a solid understanding. We have experience in a number of
programming languages, but little understanding of C++. Unreal Engine’s
C++ can be easier to learn than standalone C++, but we should still
understand the language before continuing.

Goals
-----

1.  A simple numeric guessing game on the Command Line/Console, written
    in C++

    a.  A random number between 0-99 (inclusive) is selected by the
        program to be guessed by the user

    b.  The user is prompted to enter a number until they do so

        i.  Ignoring non-numeric input, and prompting the user to try
            again

    c.  Once a number has been given, it is checked for correctness

        i.  If correct, the game ends, and the user is told how many
            guesses they made

    d.  If not correct, the user is told whether the number they are
        seeking is smaller or larger than the input number

Personnel
---------

In this section, list the primary author of the spike report, as well as
any personnel who assisted in completing the work.

  ------------------- -----------------
  Primary – Jared K   Secondary – N/A
  ------------------- -----------------

Technologies, Tools, and Resources used
---------------------------------------

Provide resource information for team members who wish to learn
additional information from this spike.

-   C++ reference: <http://en.cppreference.com/w/>

-   C++ random number generator:
    <http://www.cplusplus.com/reference/cstdlib/rand/>

-   Accepting the right input from std::cin:
    <http://www.cplusplus.com/forum/beginner/21595/>

-   Random number generator seed:
    <http://www.cplusplus.com/reference/cstdlib/srand/>

-   http://www.cplusplus.com/forum/articles/9645/

Tasks undertaken
----------------

**Imports:**

Libraries are imported by adding “#include libraryname“ at the
top of the file. The libraries used are:

1.  iostream

2.  random

3.  ctime

4.  sstream

5.  string

**Main:**

1.  C++ requires a Main() function, we declare that first and all code
    we want to be executed will be written or referenced there.

**Variables:**

The variables needed for this are:

1.  A random integer for the user to try to guess

2.  A counter for the number of attempts

3.  A string to store the user’s input. We check it for validity later,
    and convert it to an int after that

**Basic Methodology:**

1.  We use a loop for as long as the user has not correctly guessed the
    answer

2.  We prompt for input and store it in a buffer string variable.

3.  After this, the length of the input is checked, since we can easily
    check the length of a string using string.length(). (The endline
    charater is included in the buffer, so we check that the variable is
    no longer than 3 characters)

4.  We then loop through the buffer and use IsDigit() to individually
    check that each character is a numerical value.

5.  After this, we convert the string to an int using a stringstream,
    and check the answer against our random int.

6.  If any of the checks fail, we display an error and prompt the user
    to try again.

7.  If we pass all of the checks, we compare the answer o our random
    int, if they are the same, the user wins, if it is higher or lower,
    we inform the user and prompt them to try again.

What we found out
-----------------

1.  std::cin is difficult to sanitize, as it needs to output to a
    variable before it can be worked with.

2.  It is best to start with a string and convert from there, as we
    don’t risk overflowing our bounds, or entering disallowed
    characters.

3.  We use string since the user is not limited in what they can enter,
    and a string is the most versatile in that respect.

4.  Stringstream can be used to easily convert ints and strings.

 \[Optional\] Open Issues/risks
-------------------------------
