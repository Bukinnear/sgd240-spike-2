#include <iostream>
#include <random>
#include <string>
#include <ctime>
#include <sstream>
#include <cctype>

void HandleInvalidInput()
{
	// throw an error indicating that the input was invalid.
	std::cout << "\nThat is not a valid number.\n\n";

	// clear the cin buffer to avoid retaining the error 
	std::cin.clear();
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

int main()
{
	using namespace std; // use the std namespace, since we use it a lot.

	srand(time(nullptr)); // set the random number generator seed

	int randomNumber = rand() % 100; // range: 0 - 99. Slightly biased to lower numbers, since we use modulo to limit the range, but oh well.

	string buffer;

	int triesCounter = 0; // counter for the number of tries.

	bool win = false; // used to loop until the user guesses correctly. set to true when the user guesses correctly.

	while (!win) // while the user's has not guessed correctly.
	{
		cout << "Please enter a numerical value between 0 and 99:\n"; // Output instructions for the user
		cin >> buffer; // Accept input into our string buffer

		if (buffer.length() > 0 && buffer.length() < 3) // Check that the input was within range by testing it's length
		{
			bool digit = true; // check that the input is numerical
			for each (char item in buffer)
			{
				if (!std::isdigit(item))
				{
					digit = false;
				}
			}
			if (digit) // if it was
			{
				int guess; // number which will contain the result
				stringstream convert(buffer); // stringstream used for the conversion initialized with the contents of the buffer
				if (!(convert >> guess)) // give the value to guess using the characters in the string
				{
					guess = -1; // if that fails set Result to -1			   
				}
				if (guess >= 0) // if the guess was valid
				{
					triesCounter++; // increment the number of tries.

					if (guess < randomNumber) // Was the guess lower or higher?
					{
						cout << "Attempt " << triesCounter << ": Your guess was lower than the number.\n\n";
					}
					else if (guess > randomNumber)
					{
						cout << "Attempt " << triesCounter << ": Your guess was higher than the number.\n\n";
					}
					else if (guess == randomNumber) // was it equal to the random number?
					{
						win = true;
					}
					else // how did it even get here?
					{
						cout << "\nTry " << triesCounter << ": ERROR4: input was neither greater, lesser, nor equal to random number. Wat?\n";
					}
				}
				else
				{
					cout << "\nERROR3: bad input recieved - string conversion failed\nInput: " << buffer;
					HandleInvalidInput();
				}
			}
			else
			{
				cout << "\nERROR2: bad input recieved - input was not numerical (no symbols or letters allowed)\nInput: " << buffer;
				HandleInvalidInput();
			}
		}
		else
		{
			cout << "\nERROR1: bad input recieved - input was too long\nInput: " << buffer;
			HandleInvalidInput();
		}
	}

	// proper grammer
	if (triesCounter == 1)
	{
		cout << "Correct!\nIt took you " << triesCounter << " attempt.";
	}
	else
	{
		cout << "Correct!\nIt took you " << triesCounter << " attempts.";
	}

	// don't close the program until the user is done.
	string b;
	cout << "\n\n\nEnter any value to exit\n";
	cin >> b;


	return 0;
}